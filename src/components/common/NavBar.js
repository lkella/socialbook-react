import React from "react";

const data_object = {
  profile_pic: "https://i.postimg.cc/cHg22LhR/profile-pic.png",
  isOnline: true,
};

class NavBar extends React.Component {
  render() {
    return (
      <nav>
        <div className="nav-left">
          <a href="Index.html">
            <img
              src="https://i.postimg.cc/Y9nZymQq/logo2.png"
              className="logo"
              alt=""
            />
          </a>
          <ul>
            <li>
              <img
                src="https://i.postimg.cc/Fs3m1Djy/notification.png"
                alt=""
              />
            </li>
            <li>
              <img src="https://i.postimg.cc/YqGKZ8nc/inbox.png" alt="" />
            </li>
            <li>
              <img src="https://i.postimg.cc/xCzpgFjg/video.png" alt="" />
            </li>
          </ul>
        </div>
        <div className="nav-right">
          <div className="search-box">
            <img src="https://i.postimg.cc/SKtHgM6Q/search.png" alt="" />
            <input type="text" placeholder="Search" />
          </div>
          <div
            className={
              data_object.isOnline ? "nav-user-icon online" : "nav-user-icon"
            }
            onClick={() => {}}
          >
            <img src={data_object.profile_pic} alt="" />
          </div>
        </div>
        {/* <!----------------Settings Menu"-----------------------> */}
        <div className="settings-menu">
          <div id="dark-btn">
            <span></span>
          </div>
          <div className="settings-menu-inner">
            <div className="user-profile">
              <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="" />
              <div>
                <p>John Nicholson</p>
                <a href="profile.html">See your profile</a>
              </div>
            </div>
            <hr alt="" />
            <div className="user-profile">
              <img src="https://i.postimg.cc/hv3nx52s/feedback.png" alt="" />
              <div>
                <p>Give Feedback</p>
                <a href="/">Help us to improve the new design</a>
              </div>
            </div>
            <hr alt="" />
            <div className="settings-links">
              <img
                src="https://i.postimg.cc/QCcPNYRV/setting.png"
                className="settings-icon"
                alt=""
              />
              <a href="/">
                Settings & Privacy{" "}
                <img
                  src="https://i.postimg.cc/RF1dBMWr/arrow.png"
                  width="10px"
                  alt=""
                />
              </a>
            </div>
            <div className="settings-links">
              <img
                src="https://i.postimg.cc/C5tydfK6/help.png"
                className="settings-icon"
                alt=""
              />
              <a href="/">
                Help & Support
                <img
                  src="https://i.postimg.cc/RF1dBMWr/arrow.png"
                  width="10px"
                  alt=""
                />
              </a>
            </div>
            <div className="settings-links">
              <img
                src="https://i.postimg.cc/5yt1XVSj/display.png"
                className="settings-icon"
                alt=""
              />
              <a href="/">
                Display & Accessibility{" "}
                <img
                  src="https://i.postimg.cc/RF1dBMWr/arrow.png"
                  width="10px"
                  alt=""
                />
              </a>
            </div>
            <div className="settings-links">
              <img
                src="https://i.postimg.cc/PJC9GrMb/logout.png"
                className="settings-icon"
                alt=""
              />
              <a href="/">
                Logout{" "}
                <img
                  src="https://i.postimg.cc/RF1dBMWr/arrow.png"
                  width="10px"
                  alt=""
                />
              </a>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
