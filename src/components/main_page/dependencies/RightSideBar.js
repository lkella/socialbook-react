import React from "react";

const RightSideBar = () => {
  const event_data = [
    {
      date: "18",
      month: "March",
      event_name: "Social Media",
      location: "Willison Tech Park",
    },
    {
      date: "22",
      month: "June",
      event_name: "Mobile Marketing",
      location: "Willison Tech Park",
    },
  ];

  const online_data = [
    { name: "Alison Mina", link: "https://i.postimg.cc/XNPtfdVs/member-1.png" },
    {
      name: "Jackson Aston",
      link: "https://i.postimg.cc/4NhqByys/member-2.png",
    },
    { name: "Samona Rose", link: "https://i.postimg.cc/FH5qqvkc/member-3.png" },
    { name: "Mike Pérez", link: "https://i.postimg.cc/Sx65bPcP/member-4.png" },
  ];

  return (
    <div className="right-sidebar">
      {/* Events  */}
      <div className="sidebar-title">
        <h4>Events</h4>
        <a href="/">See All</a>
      </div>
      {event_data.map((event) => {
        return (
          <div className="event">
            <div className="left-event">
              <h3>{event.date}</h3>
              <span>{event.month}</span>
            </div>
            <div className="right-event">
              <h4>{event.event_name}</h4>
              <p>
                <i className="fas fa-map-marker-alt"></i> {event.location}
              </p>
              <a href="/">More Info</a>
            </div>
          </div>
        );
      })}

      {/* Advertisement */}
      <div className="sidebar-title">
        <h4>Advertisement</h4>
        <a href="/">close</a>
      </div>
      <img
        src="https://i.postimg.cc/CLXYx9BL/advertisement.png"
        className="siderbar-ads"
        alt=""
      />

      {/* Online List */}
      <div className="sidebar-title">
        <h4>Conversation</h4>
        <a href="/">Hide Chat</a>
      </div>

      {online_data.map((data) => {
        return (
          <div className="online-list">
            <div className="online">
              <img src={data.link} alt={data.name} />
            </div>
            <p>{data.name}</p>
          </div>
        );
      })}
    </div>
  );
};

export default RightSideBar;
