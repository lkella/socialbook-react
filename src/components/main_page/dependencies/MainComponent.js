import React from "react";

const NewPost = () => {
  return (
    <div className="write-post-container">
      <div className="user-profile">
        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="" />
        <div>
          <p>John Nicholson</p>
          <small>
            Public <i className="fas fa-caret-down"></i>
          </small>
        </div>
      </div>
      <div className="post-input-container">
        <textarea rows="3" placeholder="What's on your mind, John?"></textarea>
        <div className="add-post-links">
          <a href="/">
            <img src="https://i.postimg.cc/QMD2BDXs/live-video.png" alt="" />
            Live Video
          </a>
          <a href="/">
            <img src="https://i.postimg.cc/6pKKZn0D/photo.png" alt="" />
            Photo/Video
          </a>
          <a href="/">
            <img src="https://i.postimg.cc/Pf6TBCdD/feeling.png" alt="" />
            Feling/Activity
          </a>
        </div>
      </div>
    </div>
  );
};

const FeedPost = () => {
  let feed_images = [
    {
      link: "https://i.postimg.cc/9fjhGTY6/feed-image-1.png",
      description:
        "Subscribe @Vkive Tutorials Youtube Channel to watch more videos on website development and UI desings. #VkiveTutorials #YoutubeChannel",
    },
    {
      link: "https://i.postimg.cc/Xvc0xJ2p/feed-image-2.png",
      description:
        "Like and share this video with friends, tag@Vkive Tutorialsfacebook page on your post. Ask your doubts in the comments. #VkiveTutorials #YoutubeChannel",
    },
    {
      link: "https://i.postimg.cc/tJ7QXz9x/feed-image-3.png",
      description:
        "Like and share this video with friends, tag@Vkive Tutorialsfacebook page on your post. Ask your doubts in the comments. #VkiveTutorials #YoutubeChannel",
    },
    {
      link: "https://i.postimg.cc/hjDRYBwM/feed-image-4.png",
      description:
        "Like and share this video with friends, tag@Vkive Tutorialsfacebook page on your post. Ask your doubts in the comments. #VkiveTutorials #YoutubeChannel",
    },
    {
      link: "https://i.postimg.cc/ZRwztQzm/feed-image-5.png",
      description:
        "Like and share this video with friends, tag@Vkive Tutorialsfacebook page on your post. Ask your doubts in the comments. #VkiveTutorials #YoutubeChannel",
    },
  ];
  return (
    <>
      {feed_images.map((post) => {
        return (
          <div className="post-container">
            <div className="post-row">
              <div className="user-profile">
                <img
                  src="https://i.postimg.cc/cHg22LhR/profile-pic.png"
                  alt=""
                />
                <div>
                  <p>John Nicholson</p>
                  <span>June 24 2021, 13:40 pm</span>
                </div>
              </div>
              <a href="/">
                <i className="fas fa-ellipsis-v"></i>
              </a>
            </div>
            <p className="post-text">{post.description}</p>
            <img src={post.link} className="post-img" alt="" />
            <div className="post-row">
              <div className="activity-icons">
                <div>
                  <img
                    src="https://i.postimg.cc/pLKNXrMq/like-blue.png"
                    alt=""
                  />
                  120
                </div>
                <div>
                  <img
                    src="https://i.postimg.cc/rmjMymWv/comments.png"
                    alt=""
                  />
                  45
                </div>
                <div>
                  <img src="https://i.postimg.cc/T2bBchpG/share.png" alt="" />
                  20
                </div>
              </div>
              <div className="post-porfile-icon">
                <img
                  src="https://i.postimg.cc/cHg22LhR/profile-pic.png"
                  alt=""
                />
                <i className="fas fa-caret-down"></i>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

class MainContent extends React.Component {
  story_data = [
    {
      name: "Post Story",
      link: "https://i.postimg.cc/TPh453Zz/upload.png",
      class: "story story1",
    },
    {
      name: "Allison",
      link: "https://i.postimg.cc/XNPtfdVs/member-1.png",
      class: "story story2",
    },
    {
      name: "Jackson",
      link: "https://i.postimg.cc/4NhqByys/member-2.png",
      class: "story story3",
    },
    {
      name: "Samona",
      link: "https://i.postimg.cc/FH5qqvkc/member-3.png",
      class: "story story4",
    },
    {
      name: "John Doe",
      link: "https://i.postimg.cc/Sx65bPcP/member-4.png",
      class: "story story5",
    },
  ];

  render() {
    return (
      <div className="main-content">
        {/* Story Gallery */}
        <div className="story-gallery">
          {this.story_data.map((data) => {
            return (
              <div className={data.class}>
                <img src={data.link} alt={data.name} />
                <p>{data.name}</p>
              </div>
            );
          })}
        </div>
        <NewPost />
        <FeedPost />
        <button type="button" className="load-more-btn">
          Load More
        </button>
      </div>
    );
  }
}

export default MainContent;
