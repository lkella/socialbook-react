import React from "react";

import "./index.css";
import NavBar from "../common/NavBar";
import Footer from "../common/Footer";
import LeftSideBar from "./dependencies/LeftSideBar";
import RightSideBar from "./dependencies/RightSideBar";
import MainContent from "./dependencies/MainComponent";

/**
 * This is a class representing the main page of the application.
 */
class Main extends React.Component {
  render() {
    return (
      <>
        <NavBar />
        <div className="container">
          <LeftSideBar />
          <MainContent />
          <RightSideBar />
        </div>
        <Footer />
      </>
    );
  }
}

export default Main;
